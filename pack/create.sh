#!/bin/sh
exec 1>&2  # send everything to log

srcdir="$(cat DIBS_DIR_SRC)"
cp "$srcdir"/suexec /

user="${1:-"foo"}"
group="${2:-"foo"}"
home="${3:-"/mnt"}"

addgroup    "$group"
adduser  -G "$group" -h "$home" -D -H "$user"

apk update
apk add --no-cache su-exec sudo
printf '%s ALL=(ALL) NOPASSWD: ALL\n' "$user" > /etc/sudoers.d/user
